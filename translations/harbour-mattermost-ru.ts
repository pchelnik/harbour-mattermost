<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <source>This is unofficial client for</source>
        <translation>Это неофициальный клиент для</translation>
    </message>
    <message>
        <source>server.</source>
        <translation>сервера.</translation>
    </message>
    <message>
        <source>Thanks to</source>
        <translation>Спасибо</translation>
    </message>
    <message>
        <source>Russian SailfishOS Community channel</source>
        <translation>каналу Русскоязычного SailfishOS сообщества</translation>
    </message>
    <message>
        <source>in Telegram for their help.</source>
        <translation>в Telegram за их помощь.</translation>
    </message>
    <message>
        <source>Sources: </source>
        <translation>Исходный код: </translation>
    </message>
    <message>
        <source>If you want to donate, you can do that by:</source>
        <translation>Пожертвования приветствуются:</translation>
    </message>
    <message>
        <source>Yandex Money</source>
        <translation>Яндекс Деньги</translation>
    </message>
    <message>
        <source>Using Emoji from</source>
        <translation>Emoji взяты отсюда:</translation>
    </message>
    <message>
        <source>Emoji categories icon design - </source>
        <translation>Дизайн значков категорий эмоджи - </translation>
    </message>
    <message>
        <source> from Junnxy studio (links below):</source>
        <translation> из Junnxy studio (ссылки ниже)</translation>
    </message>
    <message>
        <source>Thanks to users (contribute and testing):</source>
        <translation>А так же пользователям (тестирование и разработка):</translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <source>Accounts</source>
        <translation>Учетные записи</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>Соединен</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>Соединение ...</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Нет связи</translation>
    </message>
    <message>
        <source>name: </source>
        <translation>Название: </translation>
    </message>
    <message>
        <source>url: </source>
        <translation>Адрес: </translation>
    </message>
    <message>
        <source>status: </source>
        <translation>Состояние: </translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Add account ...</source>
        <translation>Добавить учетную запись ...</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Включено</translation>
    </message>
    <message>
        <source>Loggining</source>
        <translation>Логирование</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
</context>
<context>
    <name>ChannelLabel</name>
    <message>
        <source>Public channes</source>
        <translation>Публичные каналы</translation>
    </message>
    <message>
        <source>Private channes</source>
        <translation>Приватные каналы</translation>
    </message>
    <message>
        <source>Direct channes</source>
        <translation>Личные сообщения</translation>
    </message>
    <message>
        <source>Typing</source>
        <translation>Печатает</translation>
    </message>
</context>
<context>
    <name>ChannelsPage</name>
    <message>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Offline</source>
        <translation>Не в сети</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка соединения</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>В сети</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>Соединение</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Авторизация</translation>
    </message>
    <message>
        <source>server custom name</source>
        <translation>Произвольное имя сервера</translation>
    </message>
    <message>
        <source>server address</source>
        <translation>Адрес сервера</translation>
    </message>
    <message>
        <source>use authentication token</source>
        <translation>Исользовать токен авторизации</translation>
    </message>
    <message>
        <source>Authentication token</source>
        <translation>Токен авторизации</translation>
    </message>
    <message>
        <source>Username or Email address</source>
        <translation>Эл. почта или имя пользователя</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Certificate options</source>
        <translation>Настройки сертификата</translation>
    </message>
    <message>
        <source>trust certificate</source>
        <translation>Доверенный сертификат</translation>
    </message>
    <message>
        <source>Choose CA certificate</source>
        <translation>Выберите сертификат ЦС</translation>
    </message>
    <message>
        <source>CA certificate path</source>
        <translation>Сертификат ЦС</translation>
    </message>
    <message>
        <source>Choose server certificate</source>
        <translation>Выберите сертификат сервера</translation>
    </message>
    <message>
        <source>Server certificate path</source>
        <translation>Сертификат сервера</translation>
    </message>
</context>
<context>
    <name>MattermostQt</name>
    <message>
        <source>Cant open CA certificate file: &quot;%0&quot;</source>
        <translation>Невозможно открыть файл центра сертификации: &quot;%0&quot;</translation>
    </message>
    <message>
        <source>Cant open certificate file: &quot;%0&quot;</source>
        <translation>Невозможно открыть файл сертификата &quot;%0&quot;</translation>
    </message>
    <message>
        <source>File: id(%0); </source>
        <translation>Файл: id(%0)</translation>
    </message>
    <message>
        <source>Image: </source>
        <translation>Изображение: </translation>
    </message>
    <message>
        <source>File: </source>
        <translation>Файл: </translation>
    </message>
    <message>
        <source>Empty message</source>
        <translation>Пустое сообщение</translation>
    </message>
</context>
<context>
    <name>MessageEditorBar</name>
    <message>
        <source>Message...</source>
        <translation>Сообщение ...</translation>
    </message>
    <message>
        <source>Uploading </source>
        <translation>Загрузка </translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>Выберите изображене</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>Выберите документ</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Выберите файл</translation>
    </message>
</context>
<context>
    <name>MessagesPage</name>
    <message>
        <source>get older</source>
        <translation>предыдущие сообщения</translation>
    </message>
    <message>
        <source>Reply</source>
        <translation>Ответить</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редатировать</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <source>Add reaction</source>
        <translation>Добавить реакцию</translation>
    </message>
</context>
<context>
    <name>OptionsPage</name>
    <message>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Show blobs</source>
        <translation>Фон сообщений</translation>
    </message>
    <message>
        <source>Show blobs unders messages</source>
        <translation>Показывать задний фон сообщений</translation>
    </message>
    <message>
        <source>Blobs opacity value</source>
        <translation>Уровень прозрачности фона</translation>
    </message>
    <message>
        <source>Markdown (beta)</source>
        <translation>Markdown (тестовый режим)</translation>
    </message>
    <message>
        <source>Use markdown formated text in messages</source>
        <translation>Исользовать Markdown форматирование сообщений</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Без отступа</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Маленький</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Средний</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Большой</translation>
    </message>
    <message>
        <source>Page padding</source>
        <translation>Отступы (от края экрана)</translation>
    </message>
    <message>
        <source>Cache size: </source>
        <translation>Размер кэша</translation>
    </message>
    <message>
        <source>Clear cache</source>
        <translation>Очистить кэш</translation>
    </message>
    <message>
        <source>Send message icon</source>
        <translation>Значек кнопки послать сообщение</translation>
    </message>
    <message>
        <source>Mail Icon</source>
        <translation>Значек &quot;Письмо&quot;</translation>
    </message>
    <message>
        <source>Send Icon</source>
        <translation>Значек &quot;Бумажный самолет&quot;</translation>
    </message>
    <message>
        <source>Reaction size</source>
        <translation>Размер реакции</translation>
    </message>
    <message>
        <source>Channels view</source>
        <translation>Список каналов</translation>
    </message>
    <message>
        <source>Search field</source>
        <translation>Поле поиска</translation>
    </message>
    <message>
        <source>Show search field in channels view</source>
        <translation>Показывать поле поиска в списке каналов</translation>
    </message>
    <message>
        <source>Messages view</source>
        <translation>Окно чата</translation>
    </message>
    <message>
        <source>Enable send photo</source>
        <translation>Вкл &quot;Добавить фото&quot;</translation>
    </message>
    <message>
        <source>Enable send Photo in attach menu</source>
        <translation>Вкл кнопку Добавить фото в меню Прикрепить</translation>
    </message>
    <message>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <source>Use logging</source>
        <translation>Использовать журнал отладки</translation>
    </message>
    <message>
        <source>Use logging in to standart</source>
        <translation>Выводить лог в</translation>
    </message>
    <message>
        <source>output</source>
        <translation>стандартный вывод</translation>
    </message>
    <message>
        <source>Log level (to stderr)</source>
        <translation>Уровень журналирования</translation>
    </message>
</context>
<context>
    <name>ParentMessageItem</name>
    <message>
        <source>Reply to</source>
        <translation>Ответить</translation>
    </message>
    <message>
        <source>Edit message from</source>
        <translation>Редактировать сообщение</translation>
    </message>
    <message>
        <source>Answer to message from</source>
        <translation>Ответ на сообщение от</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Login failed because of invalid password</source>
        <translation>Не удалось подключиться к серверу, неверный пароль</translation>
    </message>
    <message>
        <source>%0 bytes</source>
        <translation>%0 байт</translation>
    </message>
    <message>
        <source>%0 Kb</source>
        <translation>%0 Кб</translation>
    </message>
    <message>
        <source>%0 Mb</source>
        <translation>%0 Мб</translation>
    </message>
    <message>
        <source>(you)</source>
        <translation>(вы)</translation>
    </message>
    <message>
        <source>somebody</source>
        <translation>кто то</translation>
    </message>
    <message>
        <source>(edited)</source>
        <translation>(изменено)</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>Байт</translation>
    </message>
    <message>
        <source>KB</source>
        <translation>КБ</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>МБ</translation>
    </message>
    <message>
        <source>GB</source>
        <translation>ГБ</translation>
    </message>
    <message>
        <source>TB</source>
        <translation>ТБ</translation>
    </message>
    <message>
        <source>PB</source>
        <translation>ПБ</translation>
    </message>
</context>
<context>
    <name>ReactionsPage</name>
    <message>
        <source>Reactions</source>
        <translation>Реакции</translation>
    </message>
</context>
<context>
    <name>TeamOptions</name>
    <message>
        <source>Team Options</source>
        <translation>Настройки комманды</translation>
    </message>
</context>
<context>
    <name>TeamsPage</name>
    <message>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
</context>
</TS>
